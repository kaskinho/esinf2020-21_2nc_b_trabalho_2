/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos 
 */
public class RedeSocialTest {

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    RedeSocial FicheiroPequeno;
    RedeSocial FicheiroGrande;

    public RedeSocialTest() throws Exception {
        FicheiroPequeno = new RedeSocial();
        FicheiroGrande = new RedeSocial();
        try {
            List<String> PequenoTesteP;
            PequenoTesteP = Files.lines(Paths.get("scountries.txt")).collect(Collectors.toList());
            List<String> PequenoTesteF = Files.lines(Paths.get("sborders.txt")).collect(Collectors.toList());
            List<String> PequenoTesteU = Files.lines(Paths.get("susers.txt")).collect(Collectors.toList());
            List<String> PequenoTesteR = Files.lines(Paths.get("srelationships.txt")).collect(Collectors.toList());
            FicheiroPequeno.GetGrafoCidade(PequenoTesteP, PequenoTesteF);
            FicheiroPequeno.GetMatrizUtilizador(PequenoTesteU, PequenoTesteR);
           
            List<String> GrandeTesteP;
            GrandeTesteP = Files.lines(Paths.get("bcountries.txt")).collect(Collectors.toList());
            List<String> GrandeTesteF = Files.lines(Paths.get("bborders.txt")).collect(Collectors.toList());
            List<String> GrandeTesteU = Files.lines(Paths.get("busers.txt")).collect(Collectors.toList());
            List<String> GrandeTesteR = Files.lines(Paths.get("brelationships.txt")).collect(Collectors.toList());
            FicheiroGrande.GetGrafoCidade(GrandeTesteP, GrandeTesteF);
            FicheiroGrande.GetMatrizUtilizador(GrandeTesteU, GrandeTesteR);
        } catch (IOException ex) {
            Logger.getLogger(RedeSocialTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testAmigosComunsInfluenceres() {
        System.out.println("Amigos Comuns Influenceres= ??");
        int p = 2;
        int g = 6;
        TreeSet<Utilizadores> ResulEsperado1 = new TreeSet();
        ResulEsperado1.add(new Utilizadores("u3", 20, new Paises("equador", "americasul", "quito", 14.88, -0.2298500, -78.5249500)));
        ResulEsperado1.add(new Utilizadores("u9", 155, new Paises("bolivia", "americasul", "lapaz", 206.12, -15.7797200, -47.9297200)));
        ResulEsperado1.add(new Utilizadores("u32", 155, new Paises("brasil", "americasul", "brasilia", 206.12, -15.7797200, -47.9297200)));
        TreeSet<Utilizadores> ResulReal1 = FicheiroPequeno.AmigosComunsInfluenceres(p);
        assertEquals(ResulEsperado1, ResulReal1);
        
        TreeSet<Utilizadores> ResulEsperado2 = new TreeSet();
        ResulEsperado2.add(new Utilizadores("u481", 20, new Paises("dinamarca", "europa", "copenhaga", 5.75, 55.6762944, 12.5681157)));
        TreeSet<Utilizadores> ResulReal2 = FicheiroGrande.AmigosComunsInfluenceres(g);
        assertEquals(ResulEsperado2, ResulReal2);
        
        System.out.println("Amigos Comuns Influenceres= OK");
    }

    @Test
    public void testAmigosNasProximidades() {
        System.out.println("Amigos Nas Proximidades= ??");
        Utilizadores amigo1 = new Utilizadores("u55", 36, new Paises("portugal", "europa", "lisboa", 10.31, 38.7071631, -9.135517));
        Utilizadores amigo2 = new Utilizadores("u2", 18, new Paises("bolivia", "americasul", "lapaz", 9.70, -16.5000000, -68.1500000));
        int p = 1;
        int g = 2;
        TreeSet<Utilizadores> ResulEsperado1 = new TreeSet();
        ResulEsperado1.add(new Utilizadores("u90", 67, new Paises("espanha", "europa", "madrid", 46.53, 40.4166909, -3.7003454)));
        TreeSet<Utilizadores> ResulReal1 = FicheiroGrande.AmigosNasProximidades(amigo1, g);
        assertEquals(ResulEsperado1, ResulReal1);
       
        TreeSet<Utilizadores> ResulEsperado2 = new TreeSet();
        ResulEsperado2.add(new Utilizadores("u1", 27, new Paises("brasil", "americasul", "brasilia", 206.12, -15.7797200, -47.9297200)));
        ResulEsperado2.add(new Utilizadores("u20", 20, new Paises("brasil", "americasul", "brasilia", 206.12, -15.7797200, -47.9297200)));
        ResulEsperado2.add(new Utilizadores("u31", 52, new Paises("brasil", "americasul", "buenosaires", 41.67, -34.6131500, -58.3772300)));
        TreeSet<Utilizadores> ResulReal2 = FicheiroPequeno.AmigosNasProximidades(amigo2, p);
        assertEquals(ResulEsperado2, ResulReal2);
        System.out.println("Amigos Nas Proximidades=OK");
            
    }

    @Test
    public void testCaminhoMaisCurto() {
        System.out.println("Caminho Mais Curto= ??");
        int n = 2;
        Utilizadores amigo1 = new Utilizadores("u31", 52, new Paises("argentina", "americasul", "buenosaires", 41.67, -34.6131500, -58.3772300));
        Utilizadores amigo2 = new Utilizadores("u24", 13, new Paises("peru", "americasul", "lima", 28.22, -12.0431800, -77.0282400));
        LinkedList<Paises> CidadesEsperadas1 = new LinkedList();
        CidadesEsperadas1.add(new Paises("argentina", "americasul", "buenosaires", 41.67, -34.6131500, -58.3772300));
        CidadesEsperadas1.add(new Paises("bolivia", "americasul", "lapaz", 9.70, -16.5000000, -68.1500000));
        CidadesEsperadas1.add(new Paises("brasil", "americasul", "brasilia", 206.12, -15.7797200, -47.9297200));
        CidadesEsperadas1.add(new Paises("venezuela", "americasul", "caracas", 31.02, 10.4880100, -66.8791900));
        CidadesEsperadas1.add((new Paises("colombia", "americasul", "bogota", 46.86, 4.6097100, -74.0817500)));
        CidadesEsperadas1.add(new Paises("peru", "americasul", "lima", 28.82, -12.0431800, -77.0282400));
        Map<Double, LinkedList<Paises>> ResulEsperado1 = new HashMap();
        ResulEsperado1.put(12977.03992958636, CidadesEsperadas1);
        Map<Double, LinkedList<Paises>> ResulReal1 = FicheiroPequeno.CaminhoMaisCurto(amigo1, amigo2, n);
        assertEquals(ResulEsperado1.toString(), ResulReal1.toString());
        
        Utilizadores amigo3 = new Utilizadores("u55", 36, new Paises("portugal", "europa", "lisboa", 10.31, 38.7071631, -9.135517));
        Utilizadores amigo4 = new Utilizadores("u8", 45, new Paises("noruega", "europa", "oslo", 5.26, 59.9138204, 10.7387413));
        LinkedList<Paises> CidadesEsperadas2 = new LinkedList();
        CidadesEsperadas2.add(new Paises("portugal", "europa", "lisboa", 10.31, 38.7071631, -9.135517));
        CidadesEsperadas2.add(new Paises("espanha", "europa", "madrid", 46.53, 40.4166909, -3.7003454));
        CidadesEsperadas2.add(new Paises("franca", "europa", "paris", 66.99, 48.8566667, 2.3509871));
        CidadesEsperadas2.add(new Paises("belgica", "europa", "bruxelas", 11.37, 50.8462807, 4.3547273));
        CidadesEsperadas2.add(new Paises("holanda", "europa", "amsterdam", 17.08, 52.3738007, 4.8909347));
        CidadesEsperadas2.add(new Paises("alemanha", "europa", "berlim", 82.8, 52.5234051, 13.4113999));
        CidadesEsperadas2.add(new Paises("polonia", "europa", "varsovia", 38.42, 52.2296756, 21.0122287));
        CidadesEsperadas2.add(new Paises("russia", "europa", "moscovo", 146.5, 55.755786, 37.617633));
        CidadesEsperadas2.add(new Paises("noruega", "europa", "oslo", 5.26, 59.9138204, 10.7387413));
        Map<Double, LinkedList<Paises>> ResulEsperado2 = new HashMap();
        ResulEsperado2.put(5882.22679580586, CidadesEsperadas2);
        Map<Double, LinkedList<Paises>> ResulReal2 = FicheiroGrande.CaminhoMaisCurto(amigo3, amigo4, n);
        assertEquals(ResulEsperado2.toString(), ResulReal2.toString());
        System.out.println("Caminho Mais Curto= OK");
    }

    @Test
    public void testNMinimoLigacoes() {
        System.out.println("NMinimo Ligacoes= ??");
        int ResulEsperado1 = 5;
        int ResulReal1 = FicheiroPequeno.NMinimoLigacoes();
        assertEquals(ResulEsperado1, ResulReal1);
        int ResulEsperado2 = 0;
        int ResulReal2 = FicheiroGrande.NMinimoLigacoes();
        assertEquals(ResulEsperado2, ResulReal2);
        System.out.println("NMinimo Ligacoes= OK");
    }

    @Test
    public void testMaiorCentralidade() {
        System.out.println("Maior Centralidade= ??");
        int n = 2;
        double p = 10.0;
        TreeSet<MaiorCentralidade> ResulEsperado1 = new TreeSet();
        ResulEsperado1.add(new MaiorCentralidade(new Paises("brasil", "americasul", "brasilia", 206.12, -15.7797200, -47.9297200), 2847.75728981067));
        ResulEsperado1.add(new MaiorCentralidade(new Paises("colombia", "americasul", "bogota", 46.86, 4.6097100, -74.0817500), 3071.363504571991));
        TreeSet<MaiorCentralidade> ResulReal1 = FicheiroPequeno.MaiorCentralidade(n, p);
        assertEquals(ResulEsperado1.toString(), ResulReal1.toString());
        double g = 0.0;
        TreeSet<MaiorCentralidade> ResulEsperado2 = new TreeSet();
        ResulEsperado2.add(new MaiorCentralidade(new Paises("eslovaquia","europa","bratislava", 5.44, 48.1483765, 17.1073105), 1213.2074327257824));
        ResulEsperado2.add(new MaiorCentralidade(new Paises("austria", "europa", "viena", 8.77, 48.2092062, 16.3727778), 1225.1220300007703));
        TreeSet<MaiorCentralidade> ResulReal2 = FicheiroGrande.MaiorCentralidade(n, g);
        assertEquals(ResulEsperado2.toString(), ResulReal2.toString());
        System.out.println("Maior Centralidade= OK");
    }
}

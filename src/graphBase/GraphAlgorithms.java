/*
* A collection of graph algorithms.
 */
package graphBase;

import java.lang.reflect.Array;
import static java.lang.reflect.Array.set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<>();

        LinkedList<V> temp = new LinkedList<>();

        resultQueue.add(vert);
        temp.add(vert);

        while (!temp.isEmpty()) {
            V currentVert = temp.remove();

            Iterable<Edge<V, E>> edges = g.outgoingEdges(currentVert);

            for (Edge<V, E> edge : edges) {

                V destVert = edge.getVDest();

                if (!resultQueue.contains(destVert)) {

                    resultQueue.add(destVert);
                    temp.add(destVert);
                }
            }
        }

        return resultQueue;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, LinkedList<V> qdfs) {

        qdfs.add(vOrig);

        Iterable<Edge<V, E>> edges = g.outgoingEdges(vOrig);

        for (Edge<V, E> edge : edges) {

            V destVert = edge.getVDest();

            if (!qdfs.contains(destVert)) {
                DepthFirstSearch(g, destVert, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }
        LinkedList<V> qdfs = new LinkedList<>();
        DepthFirstSearch(g, vert, qdfs);
        return qdfs;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {

        path.push(vOrig);

        Iterable<V> vertices = g.adjVertices(vOrig);
        for (V vAdj : vertices) {
            if (vAdj.equals(vDest)) {
                path.push(vAdj);
                paths.add(path);
                path.pop();
            } else {
                if (!path.contains(vAdj)) {
                    allPaths(g, vAdj, vDest, path, paths);
                }
            }
        }

        path.pop();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V voInf, V vdInf) {
        if (!g.validVertex(voInf) || !g.validVertex(vdInf)) {
            return null;

        }

        LinkedList<V> path = new LinkedList<>();

        ArrayList<LinkedList<V>> paths = new ArrayList<>();

        allPaths(g, voInf, vdInf, path, paths);

        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig,
            boolean[] visited, V[] pathKeys, double[] dist) {

        Iterable<V> verticesIterator = g.vertices();
        for (V v : verticesIterator) {
            int index = g.getKey(v);
            dist[index] = Double.MAX_VALUE;
            visited[index] = false;
            pathKeys[index] = null;
        }

        dist[g.getKey(vOrig)] = 0;

        while (vOrig != null) {
            visited[g.getKey(vOrig)] = true;

            verticesIterator = g.adjVertices(vOrig);
            for (V vAdj : verticesIterator) {
                Edge<V, E> edge = g.getEdge(vOrig, vAdj);
                int vadj = g.getKey(vAdj);

                if (visited[vadj] == false && dist[vadj] > (dist[g.getKey(vOrig)] + edge.getWeight())) {
                    dist[vadj] = (dist[g.getKey(vOrig)] + edge.getWeight());
                    pathKeys[vadj] = vOrig;
                }
            }
            vOrig = getVertMinDist(g, dist, visited);
        }
    }

    public static <V, E> V getVertMinDist(Graph<V, E> g, double[] dist, boolean[] visited) {
        V vOrig = null;
        double min = Double.MAX_VALUE;
        List<V> myList = new ArrayList();

        for (V vAdj : g.vertices()) {
            myList.add(vAdj);

        }

        for (int i = 0; i < dist.length; i++) {
            if (dist[i] < min && dist[i] != 0 && visited[i] == false) {
                min = dist[i];
                vOrig = myList.get(i);
            }
        }

        return vOrig;
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] pathKeys, LinkedList<V> path) {

        if (vDest != null) {
            path.addFirst(vDest);

            if (g.getKey(vOrig) != g.getKey(vDest)) {
                vDest = pathKeys[g.getKey(vDest)];
                getPath(g, vOrig, vDest, pathKeys, path);
            }

        }
    }

    //shortest-path between vOrig and vDest
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return 0;
        }

        int numVert = g.numVertices();

        boolean visited[] = new boolean[numVert];
        List<V> myList = new ArrayList();

        for (V vAdj : g.vertices()) {
            myList.add(vAdj);

        }
        V[] pathKeys = (V[]) myList.toArray();

        int destIdx = g.getKey(vDest);

        boolean[] knownVertices = new boolean[g.numVertices()];

        double[] minDist = new double[g.numVertices()];

        shortestPathLength(g, vOrig, knownVertices, pathKeys, minDist);

        if (minDist[destIdx] == -1 || minDist[destIdx] == Double.MAX_VALUE) {
            return 0;
        }
        getPath(g, vOrig, vDest, pathKeys, shortPath);
        revPath(shortPath);
        if (minDist[destIdx] == 0) {
            return 0;
        }

        return minDist[destIdx];

    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}

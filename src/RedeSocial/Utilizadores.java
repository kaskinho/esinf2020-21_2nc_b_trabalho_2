/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

import java.util.Objects;

/**
 *
 * @author Carlos
 */
public class Utilizadores implements Comparable<Utilizadores>{

    String UtilizadorI;
    int idade;
    Paises endereco;

    public Utilizadores(String UtilizadorI, int idade, Paises endereco) {
        this.UtilizadorI = UtilizadorI;
        this.idade = idade;
        this.endereco = endereco;
    }

    Utilizadores(Utilizadores user) {
       this.UtilizadorI = user.getUtilizadorI();
        this.idade = user.getIdade();
        this.endereco = user.endereco;
    }

    public String getUtilizadorI() {
        return UtilizadorI;
    }

    public int getIdade() {
        return idade;
    }

    public Paises getEndereco() {
        return endereco;
    }

    public void setUtilizadorI(String UtilizadorI) {
        this.UtilizadorI = UtilizadorI;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setEndereco(Paises endereco) {
        this.endereco = endereco;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.UtilizadorI);
        hash = 83 * hash + this.idade;
        hash = 83 * hash + Objects.hashCode(this.endereco);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilizadores other = (Utilizadores) obj;
        if (this.idade != other.idade) {
            return false;
        }
        if (!Objects.equals(this.UtilizadorI, other.UtilizadorI)) {
            return false;
        }
        if (!Objects.equals(this.endereco, other.endereco)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  "Utilizador = " + UtilizadorI + ", idade = " + idade + " anos, Morada = " + endereco + '}';
    }
         
    @Override
    public int compareTo(Utilizadores o) {
        return this.UtilizadorI.compareTo(o.UtilizadorI);
    }
}

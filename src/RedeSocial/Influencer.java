/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

/**
 *
 * @author Carlos
 */
public class Influencer implements Comparable<Influencer>{
    Utilizadores utlz;
    int valor;

    public Influencer(Utilizadores user, int valor) {
        this.utlz = new Utilizadores(user);
        this.valor = valor;
    }

    public Utilizadores getUtlz() {
        return utlz;
    }

    public int getValor() {
        return valor;
    }

    public void setUtlz(Utilizadores utlz) {
        this.utlz = utlz;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public int compareTo(Influencer t) {
        if (this.valor == t.getValor()) {
            return this.utlz.UtilizadorI.compareTo(t.getUtlz().getUtilizadorI());
        } else {
            return  t.getValor()-this.valor;
        }
    }

    @Override
    public String toString() {
        return "Influencer{" + "utlz=" + utlz + ", valor=" + valor + '}';
    }
}
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

/**
 *
 * @author Carlos
 */
public class MaiorCentralidade implements Comparable<MaiorCentralidade>  {
    
    Paises cidade;
    double MediaCidade;

    public MaiorCentralidade(Paises cidade, double mediaCidades) {
        this.cidade = cidade;
        this.MediaCidade = mediaCidades;
    }

    public Paises getCidade() {
        return cidade;
    }

    public double getMediaCidade() {
        return MediaCidade;
    }

    @Override
    public int compareTo(MaiorCentralidade t) {
        if(this.MediaCidade-t.MediaCidade > 0) return 1;
        if(this.MediaCidade-t.MediaCidade < 0) return -1;
            return 0;
    }

    public void setCidade(Paises cidade) {
        this.cidade = cidade;
    }

    public void setMediaCidade(double MediaCidade) {
        this.MediaCidade = MediaCidade;
    }

    @Override
    public String toString() {
        return "MaiorCentralidade{" + "cidade = " + cidade.capital + ", Media Cidade = " + MediaCidade + '}';
    }
 }
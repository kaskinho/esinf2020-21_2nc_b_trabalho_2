/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

/**
 *
 * @author Carlos
 */
public class MaisAmigosPorCidade implements Comparable<MaisAmigosPorCidade>  {
    Paises cidade;
    int valor;

    public MaisAmigosPorCidade(Paises cidade, int valor) {
        this.cidade = cidade;
        this.valor = valor;
    }

    public Paises getCidade() {
        return cidade;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public int compareTo(MaisAmigosPorCidade t) {
        if (this.valor == t.getValor()) {
            return this.cidade.capital.compareTo(t.cidade.getCapital());
        } else {
            return  t.getValor()-this.valor;
        }
    }
}
    

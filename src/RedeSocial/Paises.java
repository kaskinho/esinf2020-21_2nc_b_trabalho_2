/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

import java.util.Objects;

/**
 *
 * @author Carlos 
 */
public class Paises implements Comparable<Paises>{
        
    String pais;
    String continente;
    String capital;
    double populacao;
    double latitude;
    double longitude;

    public Paises(String pais, String continente, String capital, double populacao, double latitude, double longitude) {
        this.pais = pais;
        this.continente = continente;
        this.capital = capital;
        this.populacao = populacao;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPais() {
        return pais;
    }

      public String getContinente() {
        return continente;
    }

    public String getCapital() {
        return capital;
    }

    public double getPopulacao() {
        return populacao;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setPopulacao(double populacao) {
        this.populacao = populacao;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Paises other = (Paises) obj;
        if (Double.doubleToLongBits(this.populacao) != Double.doubleToLongBits(other.populacao)) {
            return false;
        }
        if (Double.doubleToLongBits(this.latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.longitude) != Double.doubleToLongBits(other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.continente, other.continente)) {
            return false;
        }
        if (!Objects.equals(this.capital, other.capital)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.pais);
        hash = 53 * hash + Objects.hashCode(this.continente);
        hash = 53 * hash + Objects.hashCode(this.capital);
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.populacao) ^ (Double.doubleToLongBits(this.populacao) >>> 32));
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        return hash;
    }
    
    @Override
    public String toString() {
        
        return "capital=" + capital;
        
//        return "Pais = " + pais + ", continente = " + continente + ", capital = " + capital + ", populacao = " + populacao + ", latitude = " + latitude + ", longitude = " + longitude + '}';
    }

    @Override
    public int compareTo(Paises t) {
        return this.capital.compareTo(t.capital);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedeSocial;

import MatrixGraph.AdjacencyMatrixGraph;
import Utils.Permutations;
import Utils.Haversine;
import graphBase.Graph;
import graphBase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.LongConsumer;
import java.util.stream.LongStream;

/**
 *
 * @author SMCMONTEIRO
 */
public class RedeSocial {
    
    AdjacencyMatrixGraph<Utilizadores, Double> MatrizUtilizadores = new AdjacencyMatrixGraph<>();
    Graph<Paises, String> MapCidades = new Graph<>(false);
    Map<Paises, Integer> NUtilizadoresPais = new TreeMap<>();

    public enum UtilizadorEnum {
        Utilizador(0), Idade(1), Capital(2);
        public int valorU;
        UtilizadorEnum(int val) {
            valorU = val;
        }
    }

    public enum PaisEnum {
        Pais(0), Continente(1), Populacao(2), Capital(3), Latitude(4), Longitude(5);
        public int valorP;
        PaisEnum(int valor) {
            valorP = valor;
        }
    }

    void GetGrafoCidade(List<String> ga, List<String> gb) throws Exception {
        for (int i = 0; i < ga.size(); i++) {
            String linha;
            String[] elementos;
            String capital;
            Paises cid;
            String continente;
            linha = ga.get(i);
            elementos = linha.split(",");
            continente = elementos[PaisEnum.Continente.valorP].replaceAll("\"", "");
            capital = elementos[PaisEnum.Capital.valorP].replaceAll("\"", "");
            cid = new Paises(elementos[PaisEnum.Pais.valorP].replaceAll("\"", ""), 
                    continente.replaceAll(" ", ""), capital.replaceAll(" ", ""),
                      Double.valueOf(elementos[PaisEnum.Populacao.valorP]),
                    Double.valueOf(elementos[PaisEnum.Latitude.valorP]), 
                    Double.valueOf(elementos[PaisEnum.Longitude.valorP]));
           if (MapCidades == null) {
                MapCidades.insertVertex(cid);
            } else {
                if (!MapCidades.validVertex(cid)) {
                    MapCidades.insertVertex(cid);
                }
            }
        }
        List<Paises> capital;
        capital = new ArrayList<>();
        MapCidades.vertices().iterator().forEachRemaining(n -> capital.add(n));
        for (int i = 0; i < gb.size(); i++) {
            String linha;
            String[] ps;
            List<Paises> tp;
            String paisA;
            String paisB;
            linha = gb.get(i);
            ps = linha.split(",");
            tp = new ArrayList<>();
            paisA = ps[0].replaceAll("\"", "");
            paisB = ps[1].replaceAll("\"", "");
            for (int y = 0; y < capital.size(); y++) {
                if (capital.get(y).getPais().equals(paisA.replaceAll(" ", ""))) tp.add(capital.get(y));
                if (capital.get(y).getPais().equals(paisB.replaceAll(" ", ""))) tp.add(capital.get(y));
            }
            if (tp.size() == 2) {
                double dist = Haversine.ConvertKM(tp.get(0).getLatitude(), tp.get(0).getLongitude(), tp.get(1).getLatitude(), tp.get(1).getLongitude());
                MapCidades.insertEdge(tp.get(0), tp.get(1), linha, dist);
            }
        }
    }
        
    void GetMatrizUtilizador(List<String> mu1, List<String> mu2) throws Exception {
        for (int i = 0; i < mu1.size(); i++) {
            String linha;
            String[] elementos;
            Paises cidade;
            Utilizadores util;
            linha = mu1.get(i);
            elementos = linha.split(",");
            cidade = BuscarCidade(elementos[UtilizadorEnum.Capital.valorU].replaceAll("\"", ""));
            util = new Utilizadores(elementos[UtilizadorEnum.Utilizador.valorU].replaceAll("\"", ""),
                    Integer.parseInt(elementos[UtilizadorEnum.Idade.valorU].replaceAll("\"", "")), cidade);
           
            if (MatrizUtilizadores == null) {
                MatrizUtilizadores.insertVertex(util);
                NUtilizadoresPais.put(cidade, 1);
            } else {
                if (!MatrizUtilizadores.checkVertex(util)) {
                    MatrizUtilizadores.insertVertex(util);
                    if (NUtilizadoresPais.containsKey(util.endereco)) NUtilizadoresPais.replace(cidade,
                            NUtilizadoresPais.get(util.endereco), (NUtilizadoresPais.get(util.endereco) + 1));
                    if (!NUtilizadoresPais.containsKey(util.endereco))  NUtilizadoresPais.put(cidade, 1);
                }
            }
        }
        List<Utilizadores> utili;
        utili = new ArrayList<>();
        MatrizUtilizadores.vertices().iterator().forEachRemaining(n -> utili.add(n));
        for (int i = 0; i < mu2.size(); i++) {
            String linha;
            String[] spl;
            List<Utilizadores> tp;
            String utlz1;
            String utlz2;
            linha = mu2.get(i);
            spl = linha.split(",");
            tp = new ArrayList<>();
            utlz1 = spl[0].replaceAll("\"", "");
            utlz2 = spl[1].replaceAll("\"", "");
            for (int y = 0; y < utili.size(); y++) {
                if (utili.get(y).getUtilizadorI().equals(utlz1.replaceAll(" ", "")))   tp.add(utili.get(y));
                if (utili.get(y).getUtilizadorI().equals(utlz2.replaceAll(" ", "")))  tp.add(utili.get(y));
             }
            if (tp.size() == 2) MatrizUtilizadores.insertEdge(tp.get(0), tp.get(1), 1.0);
        }
    }

     /**
     * 2. Devolver os amigos comuns entre os n utilizadores mais populares da rede. A popularidade de um utilizador é dada pelo seu número de amizades.
        * 
     * @param n
     * @return 
     */
    public TreeSet<Utilizadores> AmigosComunsInfluenceres(int n) {
        int a = 0;
        TreeSet<Influencer> tp;
        Iterator<Utilizadores> InfluUsers;
        tp = new TreeSet();
        InfluUsers = MatrizUtilizadores.vertices().iterator();
        while (InfluUsers.hasNext()) {
            Utilizadores utlzdr = InfluUsers.next();
            if (tp.isEmpty()) {
                tp.add(new Influencer(utlzdr, MatrizUtilizadores.inDegree(utlzdr)));
                a++;
            } else if (!tp.isEmpty()) {
                if (tp.size() >= n) {
                    for (int i = 0; i < a; i++) {
                        Influencer user1 = tp.iterator().next();
                        if (MatrizUtilizadores.inDegree(utlzdr) > MatrizUtilizadores.inDegree(user1.getUtlz())) {
                            tp.remove(tp.last());
                            tp.add(new Influencer(utlzdr, MatrizUtilizadores.inDegree(utlzdr)));
                            break;
                        }
                    }
                } else {
                    tp.add(new Influencer(utlzdr, MatrizUtilizadores.inDegree(utlzdr)));
                    a++;
                }
            }
        }
        return UtilizadoresEmComum(tp);
    }

    public TreeSet<Utilizadores> UtilizadoresEmComum(TreeSet<Influencer> TreeUtp) {
        Iterator<Influencer> UtlzdrTp;
        TreeSet<Utilizadores> TreeUtilizador;
        boolean teste = false;
        UtlzdrTp = TreeUtp.iterator();
        TreeUtilizador = new TreeSet();
        while (UtlzdrTp.hasNext()) {
            Influencer user;
            user = UtlzdrTp.next();
            if (teste) {
                TreeSet<Utilizadores> TreeUtlzdr;
                TreeUtlzdr = new TreeSet();
                MatrizUtilizadores.directConnections(user.getUtlz()).iterator().forEachRemaining(p -> TreeUtlzdr.add(p));
                List<Utilizadores> elimina;
                Iterator<Utilizadores> iterador;
                elimina = new ArrayList();
                iterador = TreeUtilizador.iterator();
                while (iterador.hasNext()) {
                    Utilizadores amigos = iterador.next();
                    if (!TreeUtlzdr.contains(amigos)) elimina.add(amigos);
                }
                TreeUtilizador.removeAll(elimina);
            }
            if (!teste) {
                Iterator<Utilizadores> ittp1 = MatrizUtilizadores.directConnections(user.getUtlz()).iterator();
                ittp1.forEachRemaining(p -> TreeUtilizador.add(p));
                teste = true;
            }
        }
        return TreeUtilizador;
    }
    /**
    * 4. Devolver para um utilizador os amigos que se encontrem nas proximidades, isto é, amigos que habitem em cidades que distam um dado número de fronteiras da cidade desse utilizador. Devolver para cada
    * cidade os respetivos amigos.
    * @param utlzr
    * @param n
    * @return CatologarUtlzr
    */
    public TreeSet<Utilizadores> AmigosNasProximidades(Utilizadores utlzr, int n) {
        TreeSet<Utilizadores> CatologarUtlzr;
        Paises cidad;
        Iterator<Paises> VerticeCdd;
        CatologarUtlzr = new TreeSet();
        cidad = utlzr.endereco;
        VerticeCdd = MapCidades.vertices().iterator();
        while (VerticeCdd.hasNext()) {
            Paises city;
            Iterator<Utilizadores> tempUsers;
            city = VerticeCdd.next();
            tempUsers = MatrizUtilizadores.directConnections(utlzr).iterator();
            while (tempUsers.hasNext()) {
                Utilizadores amigo;
                amigo = tempUsers.next();
                if (city.equals(amigo.endereco)) {
                    LinkedList<Paises> CCurto;
                    CCurto = new LinkedList<>();
                    GraphAlgorithms.shortestPath(MapCidades, cidad, city, CCurto);
                    if (CCurto.size() <= n + 1 && CCurto.size() >= 1) {
                        CatologarUtlzr.add(amigo);
                    }
                }
            }
        }
        return CatologarUtlzr;
    }
/**
 * Devolver o caminho terrestre mais curto entre dois utilizadores, passando obrigatoriamente pelas ncidade(s) intermédias onde cada utilizador tenha o maior número de amigos. Note que as cidades origem,
 * destino e intermédias devem ser todas distintas. O caminho encontrado deve indicar as cidades incluídas e a respetiva distância em km.
 * @param utilizador1
 * @param utilizador2
 * @param n
 * @return 
 */
   
    public Map<Double, LinkedList<Paises>> CaminhoMaisCurto(Utilizadores utilizador1, Utilizadores utilizador2, int n) {
        Paises cidade1;
        Paises cidade2;
        Map<Double, LinkedList<Paises>> ret;
        LinkedList<Paises> shortPath;
        cidade1 = utilizador1.endereco;
        cidade2 = utilizador2.endereco;
        ret = new HashMap();
        shortPath = new LinkedList<>();
        double Soma = 0;
        if (!cidade1.continente.equals(cidade2.continente) || MapCidades.inDegree(cidade1) == 0 || MapCidades.inDegree(cidade2) == 0 || 
                cidade1.pais.equals("reinounido") || cidade1.pais.equals("irlanda") || cidade2.pais.equals("irlanda") || cidade2.pais.equals("reinounido")) {
            return ret;
        }
        Set<Paises> Visitar = new HashSet();
        Visitar.addAll(MaisAmigosPorCidade(utilizador1, n));
        Visitar.addAll(MaisAmigosPorCidade(utilizador2, n));
        LinkedList<Paises> elimina = new LinkedList<>();
        Visitar.forEach((c1) -> {
            double v = MapCidades.inDegree(c1);                                     
            if (!cidade1.continente.equals(c1.continente) || v == 0 || c1.pais.equals("reinounido") || c1.pais.equals("irlanda")) {
                elimina.add(c1);
            }
        });
        Visitar.removeAll(elimina);
        LinkedList<List<Paises>> Apath;
        LinkedList<Paises> CEncontrado;
        Apath = new LinkedList();
        CEncontrado = new LinkedList<>();
        Visitar.iterator().forEachRemaining(t -> shortPath.add(t));
        if (shortPath.size() > 1) {
            permutacao(shortPath, Apath);
            double confere = Double.MAX_VALUE;
            for (List<Paises> lista : Apath) {
                LinkedList<Paises> pthis = new LinkedList();
                LinkedList<Paises> TpPath = new LinkedList<>();
                Soma = 0.0;
                Soma += graphBase.GraphAlgorithms.shortestPath(MapCidades, cidade1, lista.get(0), TpPath);
                for (int l = 0; l < TpPath.size() - 1; l++) {
                    pthis.add(TpPath.get(l));
                }
                TpPath.clear();
                for (int i = 0; i < lista.size() - 1; i++) {
                    Paises a = lista.get(i);
                    Paises z = lista.get((i+1));
                    Soma += graphBase.GraphAlgorithms.shortestPath(MapCidades, a, z, TpPath);
                    for (int l = 0; l < TpPath.size() - 1; l++) {
                        pthis.add(TpPath.get(l));
                    }
                    TpPath.clear();
                }
                Soma += graphBase.GraphAlgorithms.shortestPath(MapCidades, lista.get(lista.size() - 1), cidade2, TpPath);
                for (int l = 0; l < TpPath.size(); l++) {
                  pthis.add(TpPath.get(l));
                }
                if (confere > Soma) {
                    CEncontrado.clear();
                    CEncontrado.addAll(pthis);
                    confere = Soma;
                }
            }
        }
        if (shortPath.isEmpty()) {
            CEncontrado = new LinkedList<>();
            Soma = graphBase.GraphAlgorithms.shortestPath(MapCidades, cidade1, cidade2, CEncontrado);
        }
        if (shortPath.size() == 1) {
            CEncontrado = new LinkedList<>();
            LinkedList<Paises> Temp = new LinkedList<>();
            Soma = 0.0;
            Soma += graphBase.GraphAlgorithms.shortestPath(MapCidades, cidade1, shortPath.get(0), Temp);
            for (int l = 0; l < Temp.size() - 1; l++) {
                CEncontrado.add(Temp.get(l));
            }
            Temp.clear();
            Soma += graphBase.GraphAlgorithms.shortestPath(MapCidades, shortPath.get(0), cidade2, Temp);
            for (Paises c1 : Temp) {
                CEncontrado.add(c1);
            }
        }
        ret.put(Soma, CEncontrado);
        return ret;
    }

    public AdjacencyMatrixGraph<Paises, Double> MatrizCidades() {
        AdjacencyMatrixGraph<Paises, Double> cidade = new AdjacencyMatrixGraph<>();
        Iterator<Paises> cidadeV = MapCidades.vertices().iterator();
        while (cidadeV.hasNext()) {
            Paises cid1 = cidadeV.next();
            cidade.insertVertex(cid1);
        }
        cidadeV = MapCidades.vertices().iterator();
        while (cidadeV.hasNext()) {
            Paises cid = cidadeV.next();
            Iterator<Paises> cidV = MapCidades.adjVertices(cid).iterator();
            while (cidV.hasNext()) {
                Paises cid2 = cidV.next();
                cidade.insertEdge(cid, cid2, MapCidades.getEdge(cid, cid2).getWeight());
            }
        }
        return cidade;
    }
/**
 * 
 * @param local
 * @return 
 */
    public Paises BuscarCidade(String local) {
        Paises city = null;
        Iterator<Paises> cidadeV = MapCidades.vertices().iterator();
        while (cidadeV.hasNext()) {
            Paises cidade = cidadeV.next();
            if (cidade.capital.equalsIgnoreCase(local)) {
                city = cidade;
                break;
            }
        }
        return city;
    }
/**
 * 
 * @param user
 * @param n
 * @return ret
 */
    public List<Paises> MaisAmigosPorCidade(Utilizadores user, int n) {
      TreeSet<MaisAmigosPorCidade> temp = new TreeSet();
        Iterator<Utilizadores> ListaAmigos = MatrizUtilizadores.directConnections(user).iterator();
        while (ListaAmigos.hasNext()) {
            Paises cid = ListaAmigos.next().getEndereco();
            temp.add(new MaisAmigosPorCidade(cid, 0));
        }
        List<MaisAmigosPorCidade> ListaCidades = new ArrayList<>();
        temp.iterator().forEachRemaining(t -> ListaCidades.add(t));
        ListaAmigos = MatrizUtilizadores.directConnections(user).iterator();
         while (ListaAmigos.hasNext()) {
            Paises cid = ListaAmigos.next().getEndereco();
            for (MaisAmigosPorCidade maisAmigos : ListaCidades) {
                if (maisAmigos.cidade.equals(cid)) {

                    int v = ListaCidades.get(ListaCidades.indexOf(maisAmigos)).getValor() + 1;
                    ListaCidades.get(ListaCidades.indexOf(maisAmigos)).setValor(v);
                }
            }
        }
        temp.clear();
        ListaCidades.iterator().forEachRemaining(t -> temp.add(t));
        List<Paises> ret = new ArrayList();
        if (temp.size() > n) {
            int tm = 0;
            while (tm < (n)) {
                ret.add(temp.first().cidade);
                temp.remove(temp.first());
                tm++;
            }
        } else {
            for (MaisAmigosPorCidade c1 : temp) {
                ret.add(c1.cidade);
            }
        }
        return ret;

    }
    /**
     * 3. Verificar se a rede de amizades é conectada e em caso positivo devolver o número mínimo de ligações
     *  necessário para nesta rede qualquer utilizador conseguir contactar um qualquer outro utilizador.
     * @return  */
    public int NMinimoLigacoes() {
        int Nminimo = 0;
        boolean ligada = true;
        Utilizadores UtilVerify;
        AdjacencyMatrixGraph<Utilizadores, Double> ciclone;
        LinkedList<Utilizadores> verify;
        ciclone = (AdjacencyMatrixGraph<Utilizadores, Double>) MatrizUtilizadores.clone();
        UtilVerify = ciclone.vertices().iterator().next();
        verify = MatrixGraph.GraphAlgorithms.BFS(MatrizUtilizadores, UtilVerify);
        if (verify.size() != MatrizUtilizadores.numVertices()) ligada = false;
        if (ligada==true) {
            AdjacencyMatrixGraph<Utilizadores, Double> Peso;
            TreeSet<Double> organiza;
            Peso = MatrixGraph.GraphAlgorithms.transitiveClosureminDistGraph(ciclone);
            organiza = new TreeSet<>();
            Peso.edges().iterator().forEachRemaining(t -> organiza.add(t));
            Nminimo = (organiza.last()).intValue();
        }
        return Nminimo;
    }
    
    public TreeSet<MaiorCentralidade> MaiorCentralidade(int n, double p) {
        AdjacencyMatrixGraph<Paises, Double> cidade;
        AdjacencyMatrixGraph<Paises, Double> caminho;
        Map<Paises, Double> CidadeCentral;
        cidade = MatrizCidades();
        CidadeCentral = new HashMap<>();
        caminho = MatrixGraph.GraphAlgorithms.transitiveClosureminDistGraph(cidade);
        for (Paises cdd : caminho.vertices()) {
            if (NUtilizadoresPais.keySet().contains(cdd)) {
                double pp = (NUtilizadoresPais.get(cdd) /Double.valueOf( MatrizUtilizadores.numVertices())) * 100;             
                if (p <= pp) {
                    if (!cdd.capital.equals("dublin") && !cdd.capital.equals("londres")) {
                        if (MapCidades.inDegree(cdd) > 0) CidadeCentral.put(cdd, 0.0);
                    }
                }
            } else {
                if (p <= 0) {
                    if (!cdd.capital.equals("dublin") && !cdd.capital.equals("londres"))  {
                        if (MapCidades.inDegree(cdd) > 0) CidadeCentral.put(cdd, 0.0);
                    }
                }
            }
        }
        CidadeCentral.keySet().forEach((cdad) -> {
            for (Paises cdd : caminho.vertices()) {                       
                if (!cdad.equals(cdd)) {
                    if (CidadeCentral.containsKey(cdad)) {
                        double t;
                        if (caminho.getEdge(cdad, cdd) == null)  t = 0;
                         else {
                            t = caminho.getEdge(cdad, cdd);
                        }
                        t = CidadeCentral.get(cdad) + t;
                        CidadeCentral.replace(cdad, t);
                    }
                    if (!CidadeCentral.containsKey(cdad)) {
                        double t;
                        if (caminho.getEdge(cdad, cdd) == null) {
                            t = 0;
                        } else {
                            t = caminho.getEdge(cdad, cdd);
                        }
                        CidadeCentral.put(cdad, t);
                    }
                }
            }
        });
        TreeSet<MaiorCentralidade> MCentralidade;
        MCentralidade = new TreeSet();
        CidadeCentral.keySet().stream().map((city) -> {
        CidadeCentral.replace(city, CidadeCentral.get(city), (CidadeCentral.get(city) / caminho.inDegree(city)));
            return city; 
        }).forEachOrdered((cidad) -> {
            MCentralidade.add(new MaiorCentralidade(cidad, CidadeCentral.get(cidad)));
        });

        if (MCentralidade.size() > n) {
            int cont = MCentralidade.size();
            while (cont > n) {                                                 
                MCentralidade.remove(MCentralidade.last());
                cont--;
            }
        }
        return MCentralidade;
    }

    private void permutacao(LinkedList shortPath, LinkedList<List<Paises>> perpaths) {
        List<Paises> items = shortPath;
        long permutations = Permutations.factorial(items.size());
        LongStream.range(0, permutations).forEachOrdered((long i) -> {
            List<Paises> permutation = Permutations.permutation(i, items);
            perpaths.add(permutation);
        } 
        );
    }  
 
}
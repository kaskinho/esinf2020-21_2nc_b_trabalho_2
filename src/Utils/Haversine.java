/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Carlos
 */
public class Haversine {
    
    public static double ConvertKM(double Latitude1,double Longitude1, double Latitude2, double Longitude2){
     double dLat=0;
     double dLon =0;
     double rad = 6371; 
     double calc1=0;
     double calc2=0;
     
     dLat = Math.toRadians( Latitude2-Latitude1); 
     dLon = Math.toRadians(Longitude2 - Longitude1); 
        // converte para radians 
     Latitude1 = Math.toRadians(Latitude1); 
     Latitude2 = Math.toRadians(Latitude2); 
        // aplica a formula
     calc1 = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(Latitude1) * Math.cos(Latitude2); 
     calc2 = 2 * Math.asin(Math.sqrt(calc1)); 
       
     return rad * calc2; 
    
    }

}
